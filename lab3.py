#Подключение библиотек
import math
import matplotlib.pyplot as plt

while True:
	while True:
		#Проверка вводимых данных
		try:
			#Решение пользователя: начать подсчет функции или нет
			answer = int(input('Начать подсчет функции?\n1 - "ДА"\n0 - "НЕТ"\nВаш ответ: '))
			print()
			if answer == 1:
				break
			elif math.isclose(answer, 0, abs_tol = 0.01):
				exit()
			else:
				print('Ответьте "0" или "1"!\n')
		except ValueError:
			print('\nВведен неверный формат данных!\n')

	while True:
		#Проверка вводимых данных
		try:
			#Выбор функции для подсчета
			num_function = int(input('Какую функцию подсчитать?\n1) G\n2) F\n3) Y\nВаш ответ: '))
			print()
			#Проверка ответа пользователя
			if num_function < 1 or num_function > 3:
				print('Функция не найдена.\n')
			else:
				break
		except ValueError:
			print('\nВведен неверный формат данных!\n') 

	while True:
		#Проверка вводимых данных
		try:
			#Ввод границ изменения "x"
			print('Введите границы изменения "x":')
			x1 = int(input('x1 = '))
			x2 = int(input('x2 = '))
			print()
			if x1 > x2:
				print('Значение "x1" должно быть меньше значения "x2"!\n')
			else:
				break
		except ValueError:
			print('\nВведен неверный формат данных!\n')

	while True:
		#Проверка вводимых данных
		try:
			#Блок ввода данных программы
			a = float(input('Введите a: '))
			while True:
				try:
					x = float(input('Введите x: '))
					print()
					if x < x1 or x > x2:
						print('Введенное значение "x" не принадлежит заданным границам!\n')
					else:
						break
				except ValueError:
					print('\nВведен неверный формат данных!\n')
			break
		except ValueError:
			print('\nВведен неверный формат данных!\n')

	while True:
		#Проверка вводимых данных
		try:
			#Ввод кол-ва шагов изменения "x"
			count_step = int(input('Введите кол-во шагов изменения "x": '))
			if math.isclose(count_step, 0, abs_tol = 0.01) or count_step < 0:
				print('\nКол-во шагов не может быть меньше нуля или равно ему!\n')
			else:
				print()
				break
		except ValueError:
			print('\nВведен неверный формат данных!\n')

	#Подсчет длины шага
	step = (x2 - x) / count_step

	#Ввод переменной для работы цикла
	i = 0

	#Ввод данных массивов
	mas_x_func, mas_value_x, mas_value_func = [], [], []

	#Блок подсчета функции в соответствии с выбором пользователя
	if num_function == 1:
		print('Функция G')
		if math.isclose(step, 0, abs_tol = 0.01):
			#Подсчет функции G
			g1 = (-20) * a ** 2 + 28 * a * x + 3 * x ** 2
			if not math.isclose(g1, 0, abs_tol = 0.01):
				g2 = 4 * ((-4) * a ** 2 - a * x + 5 * x ** 2)
				G = g2 / g1
				print('x = {0:.5f} G = {1:.5f}'.format(x, G))
				mas_x_func.append((x, G))
			else:
				print('x = {0:.5f} Ошибка! Входные значения не принадлежат области определения функции.'.format(x))
				mas_x_func.append((x, None))
		else:	
			while i != count_step:
				#Подсчет функции G
				g1 = (-20) * a ** 2 + 28 * a * x + 3 * x ** 2
				if not math.isclose(g1, 0, abs_tol = 0.01):
					g2 = 4 * ((-4) * a ** 2 - a * x + 5 * x ** 2)
					G = g2 / g1
					print('x = {0:.5f} G = {1:.5f}'.format(x, G))
					mas_x_func.append((x, G))
				else:
					print('x = {0:.5f} Ошибка! Входные значения не принадлежат области определения функции.'.format(x))
					mas_x_func.append((x, None))
				x += step
				i += 1
				
	elif num_function == 2:
		print('Функция F')
		if math.isclose(step, 0, abs_tol = 0.01):
			#Подсчет функции F
			F = math.atan(24 * a ** 2 - 25 * a * x + 6 * x ** 2)
			print('x = {0:.5f} F = {1:.5f}'.format(x, F))
			mas_x_func.append((x, F))
		else:		
			while i != count_step:
				#Подсчет функции F
				F = math.atan(24 * a ** 2 - 25 * a * x + 6 * x ** 2)
				print('x = {0:.5f} F = {1:.5f}'.format(x, F))
				mas_x_func.append((x, F))
				x += step
				i += 1	

	else:
		print('Функция Y')
		if math.isclose(step, 0, abs_tol = 0.01):
			#Подсчет функции Y
			y1 = 2 * a ** 2 - 7 * a * x + 6 * x ** 2 + 1
			if not y1 < 0:
				Y = math.log(y1)
				print('x = {0:.5f} Y = {1:.5f}'.format(x, Y))
				mas_x_func.append((x, Y))
			else:
				print('x = {0:.5f} Ошибка! Входные значения не принадлежат области определения функции.'.format(x))	
				mas_x_func.append((x, None))	
		else:
			while i != count_step:
				#Подсчет функции Y
				y1 = 2 * a ** 2 - 7 * a * x + 6 * x ** 2 + 1
				if not y1 < 0:
					Y = math.log(y1)
					print('x = {0:.5f} Y = {1:.5f}'.format(x, Y))
					mas_x_func.append((x, Y))
				else:
					print('x = {0:.5f} Ошибка! Входные значения не принадлежат области определения функции.'.format(x))	
					mas_x_func.append((x, None))
				x += step
				i += 1

	#Разделение массива mas_x_func на 2 других: 1) со значениями "x" (mas_value_x); 2) со значениями функций в данных "x" (mas_value_func)
	for value_x in mas_x_func:
		mas_value_x.append(value_x[0])
	for value_func in mas_x_func:
		mas_value_func.append(value_func[1])

	#Вывод графика
	plt.plot(mas_value_x, mas_value_func)
	plt.title('График данного уравнения')
	plt.grid()
	plt.show()

	print()